# frozen_string_literal: true

require 'image_processing/mini_magick'

class ImageUploader < Shrine
  plugin :default_url
  plugin :derivation_endpoint,
         secret_key: Rails.application.credentials.derivation_secret_key,
         prefix: 'derivations/image'
  plugin :pretty_location
  plugin :validation_helpers

  Attacher.default_url do |**_options|
    "/#{name}/missing.jpg"
  end

  Attacher.validate do
    validate_max_size 10 * 1024 * 1024
  end

  derivation :thumbnail do |file, width, height|
    ImageProcessing::MiniMagick
      .source(file)
      .resize_to_limit!(width.to_i, height.to_i)
  end
end
