# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Turbo::Redirection
end
