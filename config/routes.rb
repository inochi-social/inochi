# frozen_string_literal: true

Rails.application.routes.draw do
  # landing page
  root to: 'landing#index'

  # mount derivation endpoint
  mount ImageUploader.derivation_endpoint => '/derivations/image'
end
