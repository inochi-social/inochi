const { environment } = require('@rails/webpacker')
const miniCssExtractPlugin = environment.plugins.get('MiniCssExtract')

// longer css hashes
miniCssExtractPlugin.options.filename = 'css/[name]-[contenthash].css'
miniCssExtractPlugin.options.chunkFilename = 'css/[name]-[contenthash].chunk.css'

module.exports = environment
